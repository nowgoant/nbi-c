#!/usr/bin/env node

'use strict';

const exec = require('../util/exec');
const logger = require('../util/logger');
// const qrcode = require('qrcode-terminal');
const commander = require('commander');
const ip = require('ip');
const chalk = require('chalk');

const program = commander
  .allowUnknownOption() //是否启动自动报错机制
  .usage('<options> [options]')
  .option('-c, --clearCache', 'clear cache for the url')
  .parse(process.argv);

const protocol = program.args[0] || '';
const port = program.args[1] || 8081;

let args1 = `${protocol}://http://${ip.address()}:${port}`;

if (args1) {
  logger.log(chalk.magenta(`port:${port}`));

  logger.log(chalk.magenta('-----------------二维码 开始-----------------------'));

  const options = ['qrcode', args1];

  if (program.clearCache) {
    options.push('-c');
  }

  exec('nbi', options, {
    stdio: 'inherit'
  });

  logger.log(chalk.magenta('------------------二维码 结束----------------------'));
}
