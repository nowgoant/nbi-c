#!/usr/bin/env node

'use strict';

const path = require('path');
const program = require('commander');
const PATH = require('../util/path');
// const chalk = require('chalk');
// const logger = require('../util/logger');
const pkg = require('./../package.json');

// hack
// https://gist.github.com/branneman/8048520#6-the-hack
process.env.NODE_PATH = (process.env.NODE_PATH || '') + ['', path.join(PATH.CWD_PATH, 'node_modules'), path.join(PATH.ROOT_PATH, 'node_modules'), PATH.HOME_DIR].join(path.delimiter);

require('module').Module._initPaths();

program
  .allowUnknownOption() //是否启动自动报错机制
  .usage('<command> [options]')
  .version(pkg.version)
  .command('qrcode <args>', '在terminal中生成二维码,例如生成URL二维码')
  .parse(process.argv);

if (!process.argv.slice(2).length) {
  program.outputHelp();
}
