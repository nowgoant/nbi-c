const path = require('path');
const homeDir = require('os').homedir();
const ROOT_PATH = path.join(__dirname, '..');
module.exports.CWD_PATH = process.cwd();
module.exports.ROOT_PATH = ROOT_PATH;
module.exports.HOME_DIR = homeDir;
module.exports.PLUGIN_PATH = path.join(homeDir, '.nbi');
